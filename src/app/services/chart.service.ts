import { Injectable } from '@angular/core';
import { Chart } from 'chart.js';
import { AppConstants } from '../utils/app.constants';

@Injectable({
    providedIn: 'root'
})

export class ChartService {

    constructor() {
    }
    buildChart(ctx, chartData) {
        if (chartData.type == AppConstants.chartDoughNut) {
            let myChart = new Chart(ctx, {

                type: 'doughnut',
                data: {
                    labels: [chartData.locality],
                    datasets: [
                        {
                            label: chartData.locality,
                            data: [chartData.income],
                            backgroundColor: ['#3e95cd'],
                            fill: false
                        },
                    ]
                },
                options: {
                    legend: {
                        display: true
                    },
                    tooltips: {
                        enabled: true
                    },
                    title: {
                        display: true,
                        text: 'Monthly income distribution',
                        position : 'bottom',
                        fontColor: '#3e95cd'
                    }
                }
            });
        } else if (chartData.type = AppConstants.chartBar) {
            //let dataToBeDisplayed = this.apiService.expenditureData.find(x=>x.pincode===560022);
            let cData = chartData.data;
            let myChart = new Chart(ctx, {
                type: 'bar',
                data: {

                    labels: ["apparel", "education", "entertainment", "finance",
                        "food", "health", "house", "other", "transport"],
                    datasets: [
                        {
                            label: 'Expenditure for ' + chartData.pincode,
                            backgroundColor: ["#3e95cd", "#3e95cd", "#3e95cd", "#3e95cd",
                                "#3e95cd", "#3e95cd", "#3e95cd", "#3e95cd", "#3e95cd"],
                            data: [cData.apparel, cData.education, cData.entertainment, cData.finance,
                            cData.food, cData.health, cData.house,
                            cData.other, cData.transport]
                        }
                    ]
                },
                options: {
                    legend: {
                        display: true,
                        legendText: ''
                    },
                    scales: {
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: 'Total expenditure in lakhs'
                            }
                        }],
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                fontColor: '#3e95cd',
                                labelString: 'Expenditure for ' + chartData.pincode,
                            }
                        }]
                    }
                }
            });

        }
    }
}