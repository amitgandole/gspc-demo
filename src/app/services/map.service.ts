import { Injectable } from '@angular/core';
import { environment } from 'src/enviornments/environment';
import * as mapboxgl from 'mapbox-gl';

@Injectable({
    providedIn: 'root'
})

export class MapService {
    map: mapboxgl.Map;
    style = 'mapbox://styles/mapbox/streets-v11';
    zoom = 12
    constructor() {
        mapboxgl.accessToken = environment.mapbox.accessToken;
    }
    buildMap(mapData) {

        this.map = new mapboxgl.Map({
            container: 'map',
            style: this.style,
            zoom: this.zoom,
            center: [mapData.geometry.rings[0][0][0], mapData.geometry.rings[0][0][1]]
        })
        this.map.on('click', 'filled-route', (e) => {
            let rowTitle = mapData.attributes.locality ? 'Locality' : 'Pincode';
            let rowTitleData = mapData.attributes.pincode ? mapData.attributes.pincode : mapData.attributes.locality;
            let table = '<table class="table table-bordered"><thead><tr><th>' + rowTitle + '</th><th>Population</th><th>Household</th></tr></thead><tbody><tr><td>' + rowTitleData + '</td><td>' + mapData.attributes.population + '</td><td>' + mapData.attributes.households + '</td></tr></tbody></table><button class="btn">View Data</button>';
            this.addPopup(e, table);
        });
        this.map.on('load', () => {
            this.drawPolyGon(mapData);
        });
        this.map.addControl(new mapboxgl.NavigationControl());
    }

    resizeMap() {
        this.map.resize();
    }

    addPopup(e, table) {
        const popup = new mapboxgl.Popup({
            closeButton: true,
            closeOnClick: true
        });
        popup.setLngLat(e.lngLat).
            setHTML(table)
            .addTo(this.map);
    }

    drawPolyGon(mapData) {
        this.map.addSource('route', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'geometry': {
                    'type': 'LineString',
                    'coordinates':
                        mapData.geometry.rings[0]
                }
            }
        });
        this.map.addLayer({
            'id': 'route',
            'type': 'line',
            'source': 'route',
            'layout': {
                'line-join': 'miter',
                'line-cap': 'butt'
            },
            'paint': {
                'line-color': '#3e95cd',
                'line-width': 4
            }
        });
        this.map.addLayer({

            'id': 'filled-route',
            'type': 'fill',
            'source': 'route',
            'layout': {},
            'paint': {
                'fill-color': '#3e95cd',
                'fill-opacity': 0.8,
                'fill-outline-color': 'black'
            }
        });

    }
}