import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable, of } from 'rxjs';


@Injectable({
    providedIn: 'root'
})

export class ApiService {

    pinCodeData: any;
    localityData: any;
    incomeData: any;
    expenditureData: any;
    constructor(private http: HttpClient) {
    }
    getJsonData() {
        const pinCodeJson = this.http.get('./assets/Json-DB/pincode.json');
        const localityJson = this.http.get('./assets/Json-DB/locality.json');
        const incomeDataJson = this.http.get('./assets/Json-DB/income.json');
        const expenditureJson = this.http.get('./assets/Json-DB/expenditure.json');

        forkJoin([pinCodeJson, localityJson, incomeDataJson, expenditureJson]).subscribe(jsonData => {
            this.pinCodeData = jsonData[0];
            this.localityData = jsonData[1];
            this.incomeData = jsonData[2];
            this.expenditureData = jsonData[3];
            
        });
    }

    getExpenditureData():Observable<any>{
        return this.http.get('./assets/Json-DB/expenditure.json');
        //return this.expenditureData;
    }
    getIncomeData():Observable<any>{
        return this.http.get('./assets/Json-DB/income.json');
        //return this.expenditureData;
    }
    getLocalityData():Observable<any>{
        return this.http.get('./assets/Json-DB/locality.json');
        //return this.expenditureData;
    }
    getPincodeData():Observable<any>{
        return this.http.get('./assets/Json-DB/pincode.json');
        //return this.expenditureData;
    }

}