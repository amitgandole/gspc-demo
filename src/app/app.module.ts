import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRouting } from './app.routing';
import { MainComponent } from './main-component/main.component';
import { MainComponentModule } from './main-component/main.module';

@NgModule({
  imports:      [ BrowserModule, FormsModule, AppRouting,MainComponentModule],
  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
