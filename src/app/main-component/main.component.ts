import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { AppConstants } from '../utils/app.constants';
import { tap } from 'rxjs/operators';
import { MapService } from '../services/map.service';

export class incomeChartDataModel {
  type: string;
  income: string;
  locality: string;
  constructor() {
    this.type = '';
    this.income = '';
    this.locality = '';
  }
}

export class expenditureChartDataModel {
  type: string;
  data: {};
  pincode: string;
  constructor() {
    this.type = '';
    this.data = {};
    this.pincode = '';
  }
}
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  currentPopulation: any;
  searchChoice = 'pin';
  mapData: any;

  constructor(private apiService: ApiService, private mapService: MapService) { }
  doughNutChart;
  barChart;
  pin;
  locality;
  expenditureChartData: expenditureChartDataModel;
  incomeChartData: incomeChartDataModel;
  metaData = {
    population: '',
    pincode: '',
    locality_id: '',
    households: '',
    district: '',
    country: 'India',
    locality: '',
    city: ''
  }


  ngOnInit() {
    this.doughNutChart = AppConstants.chartDoughNut;
    this.barChart = AppConstants.chartBar;
    // this.displayExpenditureChart();
    // this.displayIncomeChart();
    this.incomeChartData = new incomeChartDataModel();
    this.expenditureChartData = new expenditureChartDataModel();
  }
  displayExpenditureChart() {
    let exChartData = new expenditureChartDataModel();
    exChartData.data = {};
    exChartData.type = '';
    exChartData.pincode = '';
    this.expenditureChartData = exChartData;
    this.apiService.getPincodeData().subscribe(pinRes => {
      let pinData = pinRes.features.find(x => x.attributes.pincode == this.pin);
      this.displayMetaData(pinData);
      if (!pinData) {
        alert('No data available for this Pincode.');
        return;
      }
      this.apiService.getExpenditureData().subscribe(expRes => {
        this.pin = this.pin ? this.pin : expRes[0].pincode;
        let expData = expRes.find(x => x.pincode == this.pin);

        if (expData) {
          exChartData.type = AppConstants.chartBar;
          exChartData.data = expData;
          exChartData.pincode = this.pin;
          this.expenditureChartData = exChartData;
        } else {
          exChartData.data = {};
          exChartData.type = '';
          exChartData.pincode = '';
          this.expenditureChartData = exChartData;

        }
      });
    });
  }

  displayIncomeChart() {
    let locality;
    let incomeData = new incomeChartDataModel();
    this.apiService.getLocalityData().subscribe(localityData => {
      this.locality = this.locality ? this.locality : localityData.features[0].attributes.locality_i;
      locality = localityData.features.find(x => x.attributes.locality_i == this.locality);
      if (!locality) {
        alert('No data available for this locality id.');
        return;
      }
      this.displayMetaData(locality);
      this.apiService.getIncomeData().subscribe(incRes => {
        let chartData = incRes.find(x => x.locality === locality.attributes.locality);
        if (chartData) {
          incomeData.type = AppConstants.chartDoughNut;
          incomeData.income = chartData.income;
          incomeData.locality = chartData.locality;
          this.currentPopulation = locality.attributes.population;
          this.incomeChartData = incomeData;
        } else {
          this.incomeChartData = new incomeChartDataModel();
        }

      });
    });
  }

  displayMetaData(m_data) {
    this.mapData = m_data; 
    if (m_data) {
      if (this.searchChoice == 'pin') {
        this.metaData.city = m_data.attributes.city;
        this.metaData.households = m_data.attributes.households;
        this.metaData.locality = m_data.attributes.locality;
        this.metaData.population = m_data.attributes.population;
        this.metaData.locality_id = m_data.attributes.locality_i;
        this.metaData.district = m_data.attributes.district_n;
        this.metaData.pincode = m_data.attributes.pincode;

      } else if (this.searchChoice == 'locality') {
        this.metaData.city = m_data.attributes.city;
        this.metaData.households = m_data.attributes.households;
        this.metaData.locality = m_data.attributes.locality;
        this.metaData.population = m_data.attributes.population;
        this.metaData.locality_id = m_data.attributes.locality_i;
        this.metaData.pincode = m_data.attributes.pincode;
      }
    }



  }

  search() {
    //  this.mapService.resizeMap();

    if(!this.pin && !this.locality){
      alert('Please enter search key to generate report.');
      return;
    }
    this.incomeChartData = new incomeChartDataModel();
    this.expenditureChartData = new expenditureChartDataModel();
    if (this.searchChoice === 'pin') {
      this.displayExpenditureChart();
    } else if (this.searchChoice === 'locality') {
      this.displayIncomeChart();
    }

  }

}
