import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'
import { MainComponent } from './main.component'
import { MapComponent } from '../components/map/map.component'
import { HttpClientModule } from '@angular/common/http'
import { ApiService } from '../services/api.service'
import { ChartComponent } from '../components/chart/chart.component'
import { FormsModule } from '@angular/forms'

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        HttpClientModule,
        FormsModule
    ],
    declarations: [
        MainComponent,
        MapComponent,
        ChartComponent
    ],
    exports: [
        MainComponent,
        MapComponent,
        RouterModule
    ],
    providers : [
        ApiService
    ]
})
export class MainComponentModule { }