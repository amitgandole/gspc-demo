import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MainComponent } from './main-component/main.component';

@NgModule({
    imports: [
        RouterModule.forRoot([
            { path: '', redirectTo: 'main-page', pathMatch: 'full' },
            {
                path: 'main-page',
                component: MainComponent,
                outlet: 'header2'
            },
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class AppRouting {

}