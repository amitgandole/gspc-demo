import { Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { ChartService } from 'src/app/services/chart.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnChanges {

  @ViewChild('mychart', { static: true }) mychart;
  canvas: any;
  ctx: any;
  @Input() chartData;
  constructor(public chartService: ChartService, private apiService: ApiService) { }

  ngOnChanges() {
    this.canvas = this.mychart.nativeElement;
    this.ctx = this.canvas.getContext('2d');
    this.ctx.canvas.width = window.innerWidth;
    this.ctx.canvas.height = window.innerHeight;
    this.chartService.buildChart(this.ctx, this.chartData);
  }
}
