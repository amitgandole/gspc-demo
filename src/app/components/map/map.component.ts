import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { MapService } from 'src/app/services/map.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  @ViewChild('myMap', { static: true }) myMap;
  canvas: any;
  ctx: any;
  @Input() mapData;

  constructor(public mapService: MapService, private apiService: ApiService) { }

  ngOnInit() {
    this.canvas = this.myMap.nativeElement;
    this.mapService.buildMap(this.mapData)
  }
  ngAfterViewInit() {
    setTimeout(() => this.mapService.resizeMap(), 0);
  }

}
